FROM bitwalker/alpine-elixir:1.8.0

# Set exposed ports
EXPOSE 4369 5000 9999
ENV PORT=5000

ENV MIX_ENV=prod

COPY gifs_to_gifs.tar.gz ./
RUN tar -xzvf gifs_to_gifs.tar.gz

USER default

CMD ./_build/prod/rel/gifs_to_gifs/bin/gifs_to_gifs foreground
