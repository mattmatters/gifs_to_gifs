# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :gifs_to_gifs,
  ecto_repos: [GifsToGifs.Repo]

# Configures the endpoint
config :gifs_to_gifs, GifsToGifsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "7/nJh2kpaUAL0RD/qktZESNycdeI/nooOaF+4PJQI+O4qwBqbxHtaG6iu94aAKai",
  render_errors: [view: GifsToGifsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: GifsToGifs.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Swarm configuration
config :swarm,
  distribution_strategy: Swarm.Distribution.Ring

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
