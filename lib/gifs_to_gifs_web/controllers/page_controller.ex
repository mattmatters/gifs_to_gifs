defmodule GifsToGifsWeb.PageController do
  use GifsToGifsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
