defmodule GifsToGifs.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      GifsToGifsWeb.Endpoint,
      GifsToGifsWeb.Presence
    ]

    opts = [strategy: :one_for_one, name: GifsToGifs.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    GifsToGifsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
