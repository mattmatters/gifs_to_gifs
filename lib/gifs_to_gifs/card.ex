defmodule GifsToGifs.Card do
  @moduledoc "A playable card"

  @type t :: %__MODULE__{
          id: String.t(),
          name: String.t(),
          value: String.t()
        }
  defstruct id: "",
            name: "",
            value: ""
end
