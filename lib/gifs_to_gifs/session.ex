defmodule GifsToGifs.Session do
  @moduledoc "Genserver for session data"

  @delay 10

  use GenServer
  alias GifsToGifs.Game

  @doc """
  Starts the link, it should have a name and the channel it is on.
  """
  def start_link(opts) do
    name = Keyword.get(opts, :name)
    GenServer.start_link(__MODULE__, opts, name: name)
  end

  # Callbacks
  @impl true
  def init(args) do
    channel = Keyword.get(args, :channel)
    {:ok, %Game{channel: channel}}
  end

  @impl true
  def handle_call({:join, name}, from, game) do
    {game, cards} = Game.join(game, name, from)
    {:reply, {game, cards}, game}
  end

  @impl true
  def handle_call({:play_card, card}, from, game) do
    {game, card} = Game.play_card(game, from, card)
    {:reply, card, game}
  end

  # Swarm callbacks
  @impl true
  def handle_call({:swarm, :begin_handoff}, _from, state) do
    {:reply, {:resume, @delay}, state}
  end

  @impl true
  def handle_cast({:swarm, :end_handoff, _delay}, state) do
    {:noreply, state}
  end

  @impl true
  def handle_cast({:swarm, :resolve_conflict, _delay}, state) do
    {:noreply, state}
  end

  @impl true
  def handle_info({:swarm, :die}, state) do
    {:stop, :shutdown, state}
  end
end
