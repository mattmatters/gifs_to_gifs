defmodule GifsToGifs.Supervisor do
  @moduledoc """
  The game supervisor, this guy manages a game existing on a node.
  """

  use Supervisor

  def start_link() do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    children = [
      worker(GifsToGifs.Session, [], restart: :temporary)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end

  @doc """
  Registers a new worker, and creates the worker process
  """
  def register(args) do
    {:ok, _pid} = Supervisor.start_child(__MODULE__, args)
  end
end
