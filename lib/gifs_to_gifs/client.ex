defmodule GifsToGifs.Client do
  @moduledoc """
  Client for handling game state
  """

  def start(name, channel) do
    opts = [name: name, channel: channel]
    {:ok, pid} = Swarm.register_name(name, GifsToGifs.Supervisor, :register, opts)
    Swarm.join(:sessions, pid)
  end
end
