defmodule GifsToGifs.Game do
  @moduledoc """
  Game state and rules.
  """

  @starting_hand_size 7
  @cards_to_win 7

  alias GifsToGifs.{Card, Player}

  @type t :: %__MODULE__{
          channel: String.t(),
          action_cards: [Card.t()],
          deck: [Card.t()],
          used: [Card.t()],
          in_play: [Card.t()],
          players: %{String.t() => Player.t()}
        }
  defstruct channel: "",
            action_cards: [],
            deck: [],
            used: [],
            in_play: [],
            players: %{}

  def join(game, name, pid) do
    {game, hand} = deal_cards(game, @starting_hand_size)
    player = Player.new(name, pid)
    players = Map.put(game.players, player.id, player)
    {%__MODULE__{game | players: players}, hand}
  end

  def leave(game, id) do
    %__MODULE__{game | players: Map.delete(game.players, id)}
  end

  @doc """
  Plays a card, removes it from players deck, and gives them a new card
  """
  def play_card(game, player_id, card) do
    # Get player's card
    card_in_play = %{player_id: player_id, card: card, votes: 0}

    # Give new card
    {game, card} = deal_card(game)
    {%__MODULE__{game | in_play: card_in_play}, card}
  end

  @doc """
  Choose a winner, reset cards in play
  """
  def win_round(%__MODULE__{in_play: []} = game), do: game

  def win_round(%__MODULE__{in_play: cards} = game) do
    [winner | _rest] = cards = Enum.sort(cards, &(&1.votes > &2.votes))
    [act_card | rest] = game.action_cards
    player = game.players[winner.player_id]
    player = %Player{player | cards_won: [act_card | player.cards_won]}

    game = %__MODULE__{
      game
      | used: cards ++ game.used,
        players: Map.put(game.players, player.id, player),
        in_play: [],
        action_cards: rest
    }

    if player.cards_won >= @cards_to_win do
      {:winner, game, player}
    else
      {:next, game}
    end
  end

  defp deal_cards(game, amount) do
    Enum.reduce(1..amount, {game, []}, fn _, {g, c} ->
      {game, card} = deal_card(g)
      {game, [card | c]}
    end)
  end

  defp deal_card(game) do
    # Add cards if deck is empty
    game =
      if length(game.deck) == 0 do
        add_spent_cards(game)
      else
        game
      end

    [card | rest] = game.deck
    {%__MODULE__{game | deck: rest}, card}
  end

  defp add_spent_cards(%__MODULE__{deck: deck, used: used} = game) do
    %__MODULE__{game | deck: deck ++ Enum.shuffle(used), used: []}
  end
end
