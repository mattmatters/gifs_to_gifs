defmodule GifsToGifs.Player do
  @moduledoc "Player state"

  @type t :: %__MODULE__{
          id: String.t(),
          name: String.t(),
          pid: pid | nil,
          cards_won: [Card.t()]
        }
  defstruct id: "",
            name: "",
            pid: nil,
            cards_won: []

  def new(name, pid) do
    %__MODULE__{name: name, pid: pid, id: UUID.uuid4()}
  end
end
